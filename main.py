import csv
import sys
from datetime import datetime

def filter_low_sales(input_file_path, output_file_path, sales_limit=3):
    with open(input_file_path, "r") as infile, open(output_file_path, "w", newline="") as outfile:
        reader = csv.DictReader(infile)
        writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames)
        writer.writeheader()

        for row in reader:
            if int(row['Sales']) < sales_limit:
                writer.writerow(row)

if __name__ == "__main__":
    current_date = datetime.now().strftime("%Y-%m-%d")
    report_file = f"poor_sales_{current_date}.csv"
    filter_low_sales("daily_sales.csv", report_file, int(sys.argv[1]))
